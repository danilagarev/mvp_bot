from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, KeyboardButton, \
    ReplyKeyboardRemove, Poll
from telegram.error import BadRequest
from telegram.ext import Updater, CommandHandler, ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

ONE, TWO, THREE, FOUR, FIVE, \
SIX, SEVEN, FIRST, SECOND, CANCEL, EIGHT, TEN, ELEVEN, TWELVE, UNSET = range(15)

token = "1201891831:AAG2izGY37az-gSjwGAoawZBHXejzYKLky4"


class MVPBot:
    def __init__(self, token_):
        self.token = token_
        self.menu = [
            [InlineKeyboardButton("Опрос", callback_data=str(ONE))],
            [InlineKeyboardButton("Викторина", callback_data=str(SEVEN))],
            [InlineKeyboardButton("Видео", callback_data=str(TWO))],
            [InlineKeyboardButton("Гифка", callback_data=str(FOUR))],
            [InlineKeyboardButton("Местоположение", callback_data=str(FIVE))],
            [InlineKeyboardButton("Мероприятие", callback_data=str(TEN))],
            [InlineKeyboardButton("Фото", callback_data=str(SIX))],
            [InlineKeyboardButton("Бросить кубик", callback_data=str(EIGHT))],
            [InlineKeyboardButton("Поставить таймер", callback_data=str(ELEVEN))],
            [InlineKeyboardButton("Клавиатура", callback_data=str(THREE))]
        ]
        self.reply_keyboard = [[KeyboardButton('Custom Keyboard')],
                               [KeyboardButton('/start')], ]
        self.cancel_keyboard = InlineKeyboardMarkup([[InlineKeyboardButton("В меню", callback_data=str(CANCEL))]])

    def start(self, update, context):
        reply_markup = InlineKeyboardMarkup(self.menu)
        update.message.reply_text(
            "Выбери возможность api",
            reply_markup=reply_markup
        )

        return FIRST

    def alarm(self, context):
        job = context.job
        context.bot.send_message(job.context, text='Beep!', reply_markup=self.cancel_keyboard)

    def set_timer(self, update, context):
        query = update.callback_query
        query.answer()
        update.effective_message.delete()
        update.effective_message.reply_text("Введи время в секундах:",
                                            reply_markup=self.cancel_keyboard)

        return SECOND

    def unset(self, update, context):
        query = update.callback_query
        query.answer()
        if 'job' not in context.chat_data:
            update.effective_message.delete()
            update.effective_message.reply_text('Нет активных таймеров')
            return SECOND

        job = context.chat_data['job']
        job.schedule_removal()
        del context.chat_data['job']
        update.effective_message.delete()
        update.effective_message.reply_text('Таймер успешно отключен!', reply_markup=self.cancel_keyboard)

        return SECOND

    def set_timer_(self, update, context):
        chat_id = update.message.chat.id
        reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton("Отключить таймер", callback_data=str(UNSET))]])
        try:
            due = int(update.message.text)
            if due < 0:
                update.message.reply_text('Введите число больше нуля!')
                return SECOND

            if 'job' in context.chat_data:
                old_job = context.chat_data['job']
                old_job.schedule_removal()
            new_job = context.job_queue.run_once(self.alarm, due, context=chat_id)
            context.chat_data['job'] = new_job

            update.message.reply_text(f'Таймер сработает через {due} секунд.', reply_markup=reply_markup)

        except (IndexError, ValueError):
            update.message.reply_text('Попробуйте ещё раз')
            return SECOND

        return SECOND

    def poll(self, update, context):
        query = update.callback_query
        query.answer()
        answers = ["Хорошо", "Очень хорошо", "Отлично", "Прекрасно"]
        update.effective_message.delete()
        update.effective_message.reply_poll("Как ты?", answers,
                                            is_anonymous=False, allows_multiple_answers=True,
                                            reply_markup=self.cancel_keyboard)
        return FIRST

    def quiz(self, update, context):
        query = update.callback_query
        query.answer()
        answers = ["Не правильно", "Правильно", "asdas"]
        update.effective_message.delete()
        update.effective_message.reply_poll("Выбери правильный вариант", answers,
                                            is_anonymous=False, type=Poll.QUIZ,
                                            correct_option_id=1,
                                            reply_markup=self.cancel_keyboard)
        return FIRST

    def video(self, update, context):
        query = update.callback_query
        query.answer()
        video = 'https://www.youtube.com/watch?v=CAZWpTtMHsk'
        update.effective_message.delete()
        update.effective_message.reply_text(video,
                                            reply_markup=self.cancel_keyboard,
                                            )
        return FIRST

    def picture(self, update, context):
        query = update.callback_query
        query.answer()
        pic = 'http://cdn29.us1.fansshare.com/' \
              'images/wallpapers1080p/' \
              'hd-nature-wallpapers-pnature-full-hd-wallpapers-hd-wallpapers-fan-top-socysvtb-143487898.jpg'
        update.effective_message.delete()
        update.effective_message.reply_photo(pic,
                                             reply_markup=self.cancel_keyboard)
        return FIRST

    def gif(self, update, context):
        query = update.callback_query
        query.answer()
        gif = 'i.gifer.com/LLuC.gif'
        update.effective_message.delete()
        update.effective_message.reply_animation(gif,
                                                 reply_markup=self.cancel_keyboard)
        return FIRST

    def dice(self, update, context):
        query = update.callback_query
        query.answer()
        update.effective_message.delete()
        update.effective_message.reply_dice(
            reply_markup=self.cancel_keyboard)
        return FIRST

    def location(self, update, context):
        query = update.callback_query
        query.answer()
        update.effective_message.delete()
        update.effective_message.reply_location(latitude=55.913810,
                                                longitude=37.749929,
                                                reply_markup=self.cancel_keyboard)
        return FIRST

    def venue(self, update, context):
        query = update.callback_query
        query.answer()
        update.effective_message.delete()
        update.effective_message.reply_venue(latitude=55.913810,
                                             longitude=37.749929,
                                             reply_markup=self.cancel_keyboard,
                                             title='Прекрасное мероприятие',
                                             address='ул.Ленина, д.1')
        return FIRST

    def keyboard(self, update, context):
        query = update.callback_query
        query.answer()
        reply_markup = ReplyKeyboardMarkup(self.reply_keyboard)
        update.effective_message.delete()
        update.effective_message.reply_text('Чтоб вернуться в меню нажмите /start',
                                            reply_markup=reply_markup)

    def cancel(self, update, context):
        query = update.callback_query
        query.answer()
        reply_markup = InlineKeyboardMarkup(self.menu)
        try:
            update.effective_message.delete()
        except BadRequest as e:
            print(e)
        update.effective_message.reply_text(
            "Выбери возможность api",
            reply_markup=reply_markup
        )
        return FIRST

    def run(self):
        updater = Updater(self.token, use_context=True)

        dp = updater.dispatcher

        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', self.start)],
            states={
                FIRST: [CallbackQueryHandler(self.poll, pattern='^' + str(ONE) + '$'),
                        CallbackQueryHandler(self.video, pattern='^' + str(TWO) + '$'),
                        CallbackQueryHandler(self.cancel, pattern='^' + str(CANCEL) + '$'),
                        CallbackQueryHandler(self.gif, pattern='^' + str(FOUR) + '$'),
                        CallbackQueryHandler(self.location, pattern='^' + str(FIVE) + '$'),
                        CallbackQueryHandler(self.picture, pattern='^' + str(SIX) + '$'),
                        CallbackQueryHandler(self.quiz, pattern='^' + str(SEVEN) + '$'),
                        CallbackQueryHandler(self.dice, pattern='^' + str(EIGHT) + '$'),
                        CallbackQueryHandler(self.venue, pattern='^' + str(TEN) + '$'),
                        CallbackQueryHandler(self.set_timer, pattern='^' + str(ELEVEN) + '$'),
                        CallbackQueryHandler(self.keyboard, pattern='^' + str(THREE) + '$')],
                SECOND: [
                    MessageHandler(Filters.text & ~(Filters.command | Filters.regex('^' + str(CANCEL) + '$')),
                                   self.set_timer_),
                    CallbackQueryHandler(self.cancel, pattern='^' + str(CANCEL) + '$'),
                    CallbackQueryHandler(self.unset, pattern='^' + str(UNSET) + '$'),
                ]
            },
            fallbacks=[CommandHandler('start', self.start)]
        )

        dp.add_handler(conv_handler)
        updater.start_polling()

        updater.idle()


if __name__ == "__main__":
    bot = MVPBot(token)
    bot.run()
